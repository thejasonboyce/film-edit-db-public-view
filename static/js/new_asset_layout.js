function toggle_visibility(id) {
       var e = document.getElementById(id);
       if(e.style.display == 'block')
          e.style.display = 'none';
       else
          e.style.display = 'block';
    }

function revealFootage() {
       var footage = document.getElementById('footage');
       var stills = document.getElementById('stills');
       var music = document.getElementById('music');
       footage.style.display = 'block';
       stills.style.display = 'none';
       music.style.display = 'none';
    }

function revealStills() {
       var footage = document.getElementById('footage');
       var stills = document.getElementById('stills');
       var music = document.getElementById('music');
       footage.style.display = 'none';
       stills.style.display = 'block';
       music.style.display = 'none';
    }

function revealMusic() {
       var footage = document.getElementById('footage');
       var stills = document.getElementById('stills');
       var music = document.getElementById('music');
       footage.style.display = 'none';
       stills.style.display = 'none';
       music.style.display = 'block';
    }