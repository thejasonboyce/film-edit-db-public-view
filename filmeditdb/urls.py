from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

import settings

urlpatterns = patterns('',
    # Examples:
    (r'^$', 'docproject.views.home'),
    (r'^projects/', include('docproject.urls')),
    (r'^accounts/', include('userprofile.urls')),
    (r'^lobby/', include('lobby.urls')),

    # url(r'^$', 'filmeditdb.views.home', name='home'),
    # url(r'^filmeditdb/', include('filmeditdb.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    # user auth urls
    url(r'^accounts/login/$', 'filmeditdb.views.login'),
    url(r'^accounts/auth/$', 'filmeditdb.views.auth_view'),
    url(r'^accounts/logout/$', 'filmeditdb.views.logout'),
    url(r'^accounts/loggedin/$', 'filmeditdb.views.loggedin'),
    url(r'^accounts/invalid/$', 'filmeditdb.views.invalid_login'),
    url(r'^accounts/register/$', 'filmeditdb.views.register_user'),
    url(r'^accounts/register_success/$', 'filmeditdb.views.register_success'),
)

if not settings.DEBUG:
	from django.contrib.staticfiles.urls import staticfiles_urlpatterns
	
	urlpatterns += staticfiles_urlpatterns()