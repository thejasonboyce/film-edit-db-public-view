from django.shortcuts import render_to_response, redirect
from django.shortcuts import get_object_or_404
from lobby.models import Post, Comment
from django.http import HttpResponse
from forms import PostForm, CommentForm
from django.http import HttpResponseRedirect
from django.core.context_processors import csrf
from django.utils import timezone
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.views.decorators.csrf import csrf_exempt
from django.template import Library
from django.db.models import Q
from django.utils import timezone
from datetime import datetime
from userprofile.models import UserProfile
import csv
from django.template import RequestContext
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

register = Library()

# Create your views here.

def home(request):
	thisuser = request.user
	following = thisuser
	post_list = Post.objects.all()
	paginator = Paginator(post_list, 20)
	next = request.path

	page = request.GET.get('page')
	try:
		posts = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		posts = paginator.page(1)
	except EmptyPage:
		# If Page is out of range (eg 9999), deliver last page of results.
		posts = paginator.page(paginator.num_pages)


	if not request.user.is_authenticated():
		followers = None
		loggedin = False
	else:
		followers = UserProfile.objects.filter(following=thisuser).count()
		loggedin = 1


	args = {'posts': posts, 'thisuser': thisuser, 'following': following, 'followers': followers, 'loggedin': loggedin, 'next': next}
	args.update(csrf(request))

	return render_to_response('lobby.html', args, context_instance=RequestContext(request))

@login_required
def new_post(request):
	thisuser = request.user
	following = thisuser
	followers = UserProfile.objects.filter(following=thisuser).count()

	if request.method == "POST":
		form = PostForm(request.POST)
		if form.is_valid():
			p = form.save(commit=False)
			p.created_by = thisuser
			form.save()
			
			return HttpResponseRedirect('/lobby/')
	else:
		form = PostForm()
		
		args = {'thisuser': thisuser, 'following': following, 'followers': followers}
		args.update(csrf(request))
		
		args['form'] = form
		
		return render_to_response('new_post.html', args)

def post(request, post_id=1):
	thisuser = request.user
	following = thisuser
	next = request.path

	if not request.user.is_authenticated():
		followers = None
		loggedin = False
	else:
		followers = UserProfile.objects.filter(following=thisuser).count()
		loggedin = 1

	args = {'post': Post.objects.get(id=post_id), 'thisuser': thisuser, 'following': following, 'followers': followers, 'next': next, 'loggedin': loggedin}
	args.update(csrf(request))

	return render_to_response('post.html', args)

@login_required
def like_post(request, post_id=1):
	thisuser = request.user
	following = thisuser
	followers = UserProfile.objects.filter(following=thisuser).count()
	post = Post.objects.get(id=post_id)

	if Post.objects.filter(id=post_id, likes=thisuser).exists():
		
		next = request.GET.get('next', None)
		if next:
			return redirect(next)
		else:
			return HttpResponseRedirect('/lobby')

	else:
		post.likes.add(thisuser)

		next = request.GET.get('next', None)
		if next:
			return redirect(next)
		else:
			return HttpResponseRedirect('/lobby')

@login_required
def edit_post(request, post_id=1):
	thisuser = request.user
	following = thisuser
	followers = UserProfile.objects.filter(following=thisuser).count()
	post = Post.objects.get(id=post_id)

	if request.method == "POST":
		if Post.objects.filter(id=post_id, created_by=thisuser).exists():
			form = PostForm(request.POST)
			if form.is_valid():
				p = form.save(commit=False)
				p.id = post.id
				p.created_by = thisuser
				p.pubdate = p.pubdate
				form.save()
				
				return HttpResponseRedirect('/lobby/')
		else:
			return HttpResponseRedirect('/lobby/')
	else:
		form = PostForm(instance=post)
		
		args = {'thisuser': thisuser, 'post': post, 'following': following, 'followers': followers}
		args.update(csrf(request))
		
		args['form'] = form
		
		return render_to_response('edit_post.html', args)

@login_required
def new_comment(request, post_id=1):
	thisuser = request.user
	following = thisuser
	followers = UserProfile.objects.filter(following=thisuser).count()
	post = Post.objects.get(id=post_id)

	if request.method == "POST":
		form = CommentForm(request.POST)
		if form.is_valid():
			c = form.save(commit=False)
			c.created_by = thisuser
			c.reply_to = post

			if Comment.objects.filter(reply_to=post_id).exists():
				latest = Comment.objects.filter(reply_to=post_id).order_by('-pubdate')[0]
				c_count = latest.id_unique if latest else 0
				c_count += 1
			else:
				c_count = 1
				
			c.id_unique = c_count

			form.save()
			
			return HttpResponseRedirect('/lobby/')
	else:
		form = CommentForm()
		
		args = {'thisuser': thisuser, 'post': post, 'following': following, 'followers': followers}
		args.update(csrf(request))
		
		args['form'] = form
		
		return render_to_response('new_comment.html', args)

@login_required
def like_comment(request, post_id=1, comment_id=1):
	thisuser = request.user
	following = thisuser
	followers = UserProfile.objects.filter(following=thisuser).count()
	post = Post.objects.get(id=post_id)
	comment = Comment.objects.get(reply_to__id=post_id, id_unique=comment_id)
	
	if Comment.objects.filter(id_unique=comment_id, reply_to__id=post_id, likes=thisuser).exists():
		
		next = request.GET.get('next', None)
		if next:
			return redirect(next)
		else:
			return HttpResponseRedirect('/lobby')

	else:
		comment.likes.add(thisuser)

		next = request.GET.get('next', None)
		if next:
			return redirect(next)
		else:
			return HttpResponseRedirect('/lobby')

@login_required
def edit_comment(request, post_id=1, comment_id=1):
	thisuser = request.user
	following = thisuser
	followers = UserProfile.objects.filter(following=thisuser).count()
	post = Post.objects.get(id=post_id)
	comment = Comment.objects.get(reply_to=post_id, id_unique=comment_id)
	next = request.path

	if request.method == "POST":
		if Comment.objects.filter(reply_to=post_id, id_unique=comment_id, created_by=thisuser).exists():
			form = CommentForm(request.POST)
			if form.is_valid():
				c = form.save(commit=False)
				c.created_by = thisuser
				c.reply_to = post
				c.pubdate = comment.pubdate
				c.id = comment.id
				c.id_unique = comment.id_unique
				form.save()
				
				return HttpResponseRedirect('/lobby/')
		else:
			return HttpResponseRedirect('/lobby/')
	else:
		form = CommentForm(instance=comment)
		
		args = {'thisuser': thisuser, 'post': post, 'comment': comment, 'following': following, 'followers': followers, 'next': next}
		args.update(csrf(request))
		
		args['form'] = form
		
		return render_to_response('edit_comment.html', args)

@login_required
def follow_user(request, user_id=1):
	thisuser = request.user
	usertofollow = User.objects.get(id=user_id)
	following = thisuser
	followers = UserProfile.objects.filter(following=thisuser).count()
	
	if UserProfile.objects.filter(user=thisuser, following=usertofollow).exists():
		
		next = request.GET.get('next', None)
		if next:
			return redirect(next)
		else:
			return HttpResponseRedirect('/lobby')

	else:
		followuser, created = UserProfile.objects.get_or_create(user=thisuser)
		followuser.following.add(usertofollow)

		next = request.GET.get('next', None)
		if next:
			return redirect(next)
		else:
			return HttpResponseRedirect('/lobby')

@login_required
def unfollow_user(request, user_id=1):
	thisuser = request.user
	usertounfollow = User.objects.get(id=user_id)
	following = thisuser
	followers = UserProfile.objects.filter(following=thisuser).count()

	if UserProfile.objects.filter(user=thisuser, following=usertounfollow).exists():
		removefollower = UserProfile.objects.get(user=thisuser, following=usertounfollow)
		removefollower.following.remove(usertounfollow)
	else:
		next = request.GET.get('next', None)
		if next:
			return redirect(next)
		else:
			return HttpResponseRedirect('/lobby')

	next = request.GET.get('next', None)
	if next:
		return redirect(next)
	else:
		return HttpResponseRedirect('/lobby')
	
def category(request, category):
	thisuser = request.user
	following = thisuser
	next = request.path

	if request.method == "POST":
		category = request.POST['category']
	else:
		category = ''
		
	post_list = Post.objects.filter(category__contains=category)
	paginator = Paginator(post_list, 20)

	page = request.GET.get('page')
	try:
		posts = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		posts = paginator.page(1)
	except EmptyPage:
		# If Page is out of range (eg 9999), deliver last page of results.
		posts = paginator.page(paginator.num_pages)

	if not request.user.is_authenticated():
		followers = None
		loggedin = False
	else:
		followers = UserProfile.objects.filter(following=thisuser).count()
		loggedin = 1
	
	args = {'posts': posts, 'thisuser': thisuser, 'following': following, 'followers': followers, 'next': next, 'loggedin': loggedin}
	args.update(csrf(request))
	
	args['category'] = category
		
	return render_to_response('lobby.html', args)
	
def tag(request, tag):
	thisuser = request.user
	following = thisuser
	next = request.path

	if not request.user.is_authenticated():
		followers = None
		loggedin = False
	else:
		followers = UserProfile.objects.filter(following=thisuser).count()
		loggedin = 1
	
	safetag = tag.replace('-', ' ')	
	post_list = Post.objects.filter(tags__icontains=safetag)
	paginator = Paginator(post_list, 20)

	page = request.GET.get('page')
	try:
		posts = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		posts = paginator.page(1)
	except EmptyPage:
		# If Page is out of range (eg 9999), deliver last page of results.
		posts = paginator.page(paginator.num_pages)
	
	args = {'posts': posts, 'thisuser': thisuser, 'following': following, 'followers': followers, 'next': next, 'loggedin': loggedin}
	args.update(csrf(request))
	
	args['tag'] = tag
		
	return render_to_response('lobby.html', args)

@login_required
def search_posts(request):
	thisuser = request.user
	if request.method == "POST":
		search_text = request.POST['search_text']
	else:
		search_text = ''
		
	searchposts = Post.objects.filter(Q(title__contains=search_text) | Q(body__contains=search_text)).distinct('pubdate')
		
	return render_to_response('ajax_search_posts.html', {'searchposts': searchposts})