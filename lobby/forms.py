from django import forms
from models import Post, Comment
from django.contrib.auth.models import User

class PostForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ('title', 'body', 'category', 'tags')
        widgets = {
        	'title': forms.TextInput(attrs={'size': '40'}),
        	'body': forms.Textarea(attrs={'rows': '3'}),
        	'tags': forms.TextInput(attrs={'size': '40'}),
        }

class CommentForm(forms.ModelForm):

	class Meta:
		model = Comment
		fields = ('comment_text',)